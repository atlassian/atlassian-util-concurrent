package io.atlassian.util.concurrent;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.function.Supplier;

import static io.atlassian.util.concurrent.Timeout.TimeSuppliers.NANOS;
import static io.atlassian.util.concurrent.Timeout.getNanosTimeout;
import static io.atlassian.util.concurrent.Timeout.timeoutFactory;
import static java.util.Objects.requireNonNull;

/**
 * Factory for creating lazily populated references.
 *
 * @since 2.1
 */
public final class Lazy {
    /**
     * Memoizing reference that is lazily computed using the supplied factory.
     * <p>
     * The {@link Supplier factory} is not held on to after it is used, enabling
     * it to be garbage collected.
     *
     * @param <T> the type.
     * @param factory for populating the initial value, called only once.
     * @return a supplier
     */
    public static <T> Supplier<T> supplier(final Supplier<T> factory) {
        return new Strong<>(factory);
    }

    /**
     * Memoizing reference that is lazily computed using the supplied factory.
     * <p>
     * The {@link Callable factory} is not held on to after it is used, enabling
     * it to be garbage collected.
     *
     * @param <T> the type.
     * @param factory for populating the initial value, called only once.
     * @return a supplier
     *
     * @since 4.1
     */
    public static <T> Supplier<T> callable(final Callable<T> factory) {
        return new Strong<>(factory);
    }

    /**
     * Memoizing reference that expires the specified amount of time after
     * creation.
     *
     * @param <T> the type.
     * @param factory for populating the initial value, called only once.
     * @param time the amount
     * @param unit the units the amount is in
     * @return a supplier
     */
    public static <T> Supplier<T> timeToLive(final Supplier<T> factory, final long time, final TimeUnit unit) {
        return new Expiring<>(factory, () -> new TimeToLive(getNanosTimeout(time, unit)));
    }

    /**
     * Memoizing reference that expires the specified amount of time after the
     * last time it was accessed.
     *
     * @param <T> the type.
     * @param factory for populating the initial value, called only once.
     * @param time the amount
     * @param unit the units the amount is in
     * @return a supplier
     */
    public static <T> Supplier<T> timeToIdle(final Supplier<T> factory, final long time, final TimeUnit unit) {
        return new Expiring<>(factory, () -> new TimeToIdle(timeoutFactory(time, unit, NANOS)));
    }

    /**
     * Returns a {@link ResettableLazyReference} which creates the value by applying the provided {@link Supplier}.
     *
     * @param factory that creates the value that will be held by the {@link ResettableLazyReference}.
     * @param <T>     the type of the contained element.
     * @return a {@link ResettableLazyReference} which creates the value by applying the provided {@link Supplier}.
     * @since 3.0
     */
    public static <T> ResettableLazyReference<T> resettable(final Supplier<T> factory) {
        return resettableCallable(factory::get);
    }

    /**
     * Returns a {@link ResettableLazyReference} which creates the value by applying the provided {@link Callable}.
     *
     * @param factory that creates the value that will be held by the {@link ResettableLazyReference}.
     * @param <T>     the type of the contained element.
     * @return a {@link ResettableLazyReference} which creates the value by applying the provided {@link Callable}.
     * @since 4.1
     */
    public static <T> ResettableLazyReference<T> resettableCallable(final Callable<T> factory) {
        return new ResettableLazyReference<T>() {
            @Override
            protected T create() throws Exception {
                return factory.call();
            }
        };
    }

    //
    // inners
    //

    /**
     * Never expires.
     */
    static final class Strong<T> extends LazyReference<T> {
        // not private for testing
        volatile Callable<T> callable;

        Strong(final Supplier<T> supplier) {
            this.callable = requireNonNull(supplier::get);
        }

        Strong(final Callable<T> callable) {
            this.callable = requireNonNull(callable);
        }

        @Override
        protected T create() throws Exception {
            try {
                return callable.call();
            } finally {
                callable = null; // not needed any more
            }
        }
    }

    /**
     * Tracks timeout from construction time
     */
    static final class TimeToLive implements Predicate<Void> {
        private final Timeout timeout;

        TimeToLive(final Timeout timeout) {
            this.timeout = timeout;
        }

        @Override
        public boolean test(final Void input) {
            return !timeout.isExpired();
        }
    }

    /**
     * Tracks timeout since last time it was asked. Once timed-out it stays that
     * way.
     */
    static final class TimeToIdle implements Predicate<Void> {
        private volatile Timeout lastAccess;
        private final Supplier<Timeout> timeout;

        TimeToIdle(final Supplier<Timeout> timeout) {
            this.timeout = requireNonNull(timeout);
            lastAccess = timeout.get();
        }

        @Override
        public boolean test(final Void input) {
            final boolean alive = !lastAccess.isExpired();
            if (alive) {
                lastAccess = timeout.get();
            }
            return alive;
        }
    }
}
