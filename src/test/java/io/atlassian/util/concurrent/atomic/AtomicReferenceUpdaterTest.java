package io.atlassian.util.concurrent.atomic;

import java.util.concurrent.atomic.AtomicReference;

import org.junit.Test;

import static org.junit.Assert.assertSame;

public class AtomicReferenceUpdaterTest {
    @Test
    public void normal() {
        final String from = "from";
        final String to = "to";
        final AtomicReference<String> ref = new AtomicReference<String>(from);
        final AtomicReferenceUpdater<String> updater = new AtomicReferenceUpdater<String>(ref) {
            public String apply(final String input) {
                assertSame(from, input);
                return to;
            }
        };
        assertSame(to, updater.update());
    }
}
