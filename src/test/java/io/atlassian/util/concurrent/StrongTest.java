package io.atlassian.util.concurrent;

import java.util.concurrent.Callable;
import java.util.function.Supplier;

import org.junit.Test;

import static java.lang.Integer.valueOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class StrongTest {

    @Test
    public void lazilyPopulated() {
        final Counter counter = new Counter();
        final Supplier<Integer> lazy = new Lazy.Strong<>(counter);
        assertEquals(0, counter.count.get());
        assertEquals(valueOf(1), lazy.get());
        assertEquals(1, counter.count.get());
    }

    @Test
    public void memoized() {
        final Counter counter = new Counter();
        final Supplier<Integer> lazy = new Lazy.Strong<>(counter);
        assertEquals(valueOf(1), lazy.get());
        assertEquals(1, counter.count.get());
        assertEquals(valueOf(1), lazy.get());
        assertEquals(1, counter.count.get());
    }

    @Test
    public void memoized_callable() {
        final Counter counter = new Counter();
        final Supplier<Integer> lazy = new Lazy.Strong<>((Callable<Integer>) counter::get);
        assertEquals(valueOf(1), lazy.get());
        assertEquals(1, counter.count.get());
        assertEquals(valueOf(1), lazy.get());
        assertEquals(1, counter.count.get());
    }

    @Test
    public void supplierIsGarbageCollectible() {
        final Lazy.Strong<Integer> lazy = new Lazy.Strong<>(new Counter());
        assertEquals(valueOf(1), lazy.get());
        assertNull(lazy.callable);
    }
}
